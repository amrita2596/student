<?php

class Subjects
{
    public function __construct()
    {
        $this->SubjectDetails();
    }
    public function SubjectDetails()
    {
        $database = new Database();
        $db = $database->connect();
        $subject = array();

        $postData = file_get_contents("php://input");
        if (isset($postData) && !empty($postData)) {
            $request = json_decode($postData);
            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $Status = mysqli_real_escape_string($db, trim($request->Status));
            $Semester = mysqli_real_escape_string($db, trim($request->Semester));
            $Department = mysqli_real_escape_string($db, trim($request->Department));

            if ($Status == "Current") {
                $query1 = "SELECT `student`.`RollNo`,
        `student`.`FirstName`,
        `student`.`LastName`,
        `student`.`Email`,
        `Student-semester`.`Department`,
        `Student-semester`.`Semester`,
        `Sem-subject`.`Subject1`,
        `Sem-subject`.`Subject2`,
        `Sem-subject`.`Subject3`,
        `Sem-subject`.`Subject4`,
        `Sem-subject`.`Lab1`,
        `Sem-subject`.`Lab2`
    FROM `student`
    INNER JOIN `Student-semester`
    ON `student`.`RollNo`=`Student-semester`.`RollNo`
    INNER JOIN `Sem-subject`
    ON `Student-semester`.`ID` = `Sem-subject`.`ID`
    WHERE `student`.`RollNo` = '{$RollNo}'";
                $result = mysqli_query($db, $query1);
                if (mysqli_num_rows($result) > 0) {
                    while ($row = mysqli_fetch_array($result)) {
                        $Subject1 = $row["Subject1"];
                        $Subject2 = $row["Subject2"];
                        $Subject3 = $row["Subject3"];
                        $Subject4 = $row["Subject4"];
                        $Lab1 = $row["Lab1"];
                        $Lab2 = $row["Lab2"];

                        $subject[] = array(
                            "Subject1" => $Subject1,
                            "Subject2" => $Subject2,
                            "Subject3" => $Subject3,
                            "Subject4" => $Subject4,
                            "Lab1" => $Lab1,
                            "Lab2" => $Lab2,
                        );
                    }
                    echo json_encode($subject);
                } else {
                    http_response_code(400);
                    echo json_encode(array("message" => "error"));
                }
            } elseif ($Status == "Completed") {
                $query2 = "SELECT `Sem-subject`.`Semester`,
                `Sem-subject`.`Subject1`,
                `Sem-subject`.`Subject2`,
                `Sem-subject`.`Subject3`,
                `Sem-subject`.`Subject4`,
                `Sem-subject`.`Lab1`,
                `Sem-subject`.`Lab2` FROM `Sem-subject` WHERE `Sem-subject`.`Department` = '{$Department}' AND `Sem-subject`.`Semester` <'{$Semester}'";
                $result = mysqli_query($db, $query2);
                if (mysqli_num_rows($result) > 0) {
                    while ($row = mysqli_fetch_array($result)) {
                        $Sem = $row["Semester"];
                        $Subject1 = $row["Subject1"];
                        $Subject2 = $row["Subject2"];
                        $Subject3 = $row["Subject3"];
                        $Subject4 = $row["Subject4"];
                        $Lab1 = $row["Lab1"];
                        $Lab2 = $row["Lab2"];

                        $subject[] = array(
                            "Semester" => $Sem,
                            "Subject1" => $Subject1,
                            "Subject2" => $Subject2,
                            "Subject3" => $Subject3,
                            "Subject4" => $Subject4,
                            "Lab1" => $Lab1,
                            "Lab2" => $Lab2,
                        );
                    }
                    echo json_encode($subject);
                } else {
                    http_response_code(400);
                    echo json_encode(array("message" => "error"));
                }
            } elseif ($Status == "Upcoming") {
                $query3 = "SELECT `Sem-subject`.`Semester`,
                `Sem-subject`.`Subject1`,
                `Sem-subject`.`Subject2`,
                `Sem-subject`.`Subject3`,
                `Sem-subject`.`Subject4`,
                `Sem-subject`.`Lab1`,
                `Sem-subject`.`Lab2` FROM `Sem-subject` WHERE `Sem-subject`.`Department` = '{$Department}' AND `Sem-subject`.`Semester` >'{$Semester}'";
                $result = mysqli_query($db, $query3);
                if (mysqli_num_rows($result) > 0) {
                    while ($row = mysqli_fetch_array($result)) {
                        $Sem = $row["Semester"];
                        $Subject1 = $row["Subject1"];
                        $Subject2 = $row["Subject2"];
                        $Subject3 = $row["Subject3"];
                        $Subject4 = $row["Subject4"];
                        $Lab1 = $row["Lab1"];
                        $Lab2 = $row["Lab2"];

                        $subject[] = array(
                            "Semester" => $Sem,
                            "Subject1" => $Subject1,
                            "Subject2" => $Subject2,
                            "Subject3" => $Subject3,
                            "Subject4" => $Subject4,
                            "Lab1" => $Lab1,
                            "Lab2" => $Lab2,
                        );
                    }
                    echo json_encode($subject);
                } else {
                    http_response_code(400);
                    echo json_encode(array("message" => "error"));
                }
            }
        } else {
            http_response_code(400);
            echo json_encode(array("message" => "no post data"));
        }
    }
}
