<?php

class Dashboard
{
    public function __construct()
    {
        $this->SemDetails();
    }
    public function SemDetails()
    {
        $database = new Database();
        $db = $database->connect();

        $postData = file_get_contents("php://input");
        if (isset($postData) && !empty($postData)) {
            $request = json_decode($postData);
            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $department = mysqli_real_escape_string($db, trim($request->Department));
            $sem = mysqli_real_escape_string($db, trim($request->Semester));

            $query = "SELECT * FROM `mydb`.`Sem-subject` WHERE `Sem-subject`.`Department` = '{$department}' AND `Sem-subject`.`Semester` = '{$sem}'";
            $result = mysqli_query($db, $query);
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_row($result);
                $returnValue = $row['0'];
                $query1 = "SELECT * FROM `mydb`.`Student-semester` WHERE `Student-semester`.`RollNo` = '{$RollNo}'";
                $result1 = mysqli_query($db, $query1);
                if (mysqli_num_rows($result1) == 0) {
                    $query2 = "INSERT INTO `mydb`.`Student-semester`(`RollNo`,`Department`,`Semester`,`ID`) VALUES ('{$RollNo}','{$department}','{$sem}','{$returnValue}')";
                    if (!mysqli_query($db, $query2)) {
                        http_response_code(401);
                        echo json_encode(array("message" => "unable to insert data"));
                    } else {
                        http_response_code(201);
                        echo json_encode(array("message" => "data inserted"));
                    }
                } else {
                    http_response_code(200);
                    echo json_encode(array("message" => "data already there"));
                }
            } else {
                http_response_code(401);
                echo json_encode(array("message" => "no row detected"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("message" => "no post data"));
        }

    }
}
