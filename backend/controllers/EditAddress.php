<?php

class EditAddress
{
    public function __construct()
    {
        $this->editAddress();
    }

    public function editAddress()
    {
        $database = new Database();
        $db = $database->connect();

        $postData = file_get_contents("php://input");
        if (isset($postData) && !empty($postData)) {
            $request = json_decode($postData);
            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $NewAddress = mysqli_real_escape_string($db, trim($request->NewAddress));
            $query = "SELECT * FROM `student` WHERE `student`.`RollNo` = '$RollNo'";
            $result = mysqli_query($db, $query);
            $row = mysqli_fetch_array($result);
            $Address = $row["Address"];
            if ($NewAddress != null && $Address != $NewAddress) {
                $query1 = "UPDATE `mydb`.`student` SET `student`.`Address`='{$NewAddress}' WHERE `student`.`RollNo`='{$RollNo}'";
                if (!mysqli_query($db, $query1)) {
                    http_response_code(400);
                    echo json_encode(array("message" => "unable to update"));
                } else {
                    http_response_code(201);
                    echo json_encode(array("message" => "address updated"));
                }
            } else {
                http_response_code(400);
                echo json_encode(array("message" => "nothing to update !"));
            }
        } else {
            http_response_code(404);
            echo json_encode(array("message" => "no post data"));
        }
    }
}
