<?php

require "./vendor/autoload.php";

class Register
{
    public function __construct()
    {
        $this->addUser();
    }

    public function addUser()
    {
        $database = new Database();
        $db = $database->connect();

        $postdata = file_get_contents("php://input");
        if (isset($postdata) && !empty($postdata)) {
            $request = json_decode($postdata);

            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $fname = mysqli_real_escape_string($db, trim($request->FirstName));
            $lname = mysqli_real_escape_string($db, trim($request->LastName));
            $address = mysqli_real_escape_string($db, trim($request->Address));
            $mobile = mysqli_real_escape_string($db, trim($request->MobileNumber));
            $email = mysqli_real_escape_string($db, trim($request->Email));
            $password = mysqli_real_escape_string($db, trim($request->Password));

            $encryptedPass = md5($password);

            $query = "INSERT INTO `mydb`.`student` (
                     `RollNo`,
                     `FirstName`,
                     `LastName`,
                     `Address`,
                     `MobileNumber`,
                     `Email`,
                    `Password`
                     ) VALUES ('{$RollNo}','{$fname}','{$lname}','{$address}','{$mobile}','{$email}','{$encryptedPass}')";

            if (!mysqli_query($db, $query)) {
                http_response_code(400);
                echo json_encode(array("message" => "unable to create user"));
            } else {
                http_response_code(201);
                echo json_encode(array("message" => "user was created"));
            }
        } else {
            http_response_code(400);
            echo json_encode(array("message" => "no post data"));
        }
    }

}
