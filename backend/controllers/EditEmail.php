<?php

class EditEmail
{
    public function __construct()
    {
        $this->editEmail();
    }

    public function editEmail()
    {
        $database = new Database();
        $db = $database->connect();

        $postData = file_get_contents("php://input");
        if (isset($postData) && !empty($postData)) {
            $request = json_decode($postData);
            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $NewEmail = mysqli_real_escape_string($db, trim($request->NewEmail));
            $query = "SELECT * FROM `student` WHERE `student`.`RollNo` = '$RollNo'";
            $result = mysqli_query($db, $query);
            $row = mysqli_fetch_array($result);
            $Email = $row["Email"];
            if ($NewEmail != null && $Email != $NewEmail) {
                $query2 = "UPDATE `mydb`.`student` SET `student`.`Email`='{$NewEmail}' WHERE `student`.`RollNo`='{$RollNo}'";
                if (!mysqli_query($db, $query2)) {
                    http_response_code(400);
                    echo json_encode(array("message" => "unable to update"));
                } else {
                    http_response_code(201);
                    echo json_encode(array("message" => "email updated"));
                }
            } else {
                http_response_code(400);
                echo json_encode(array("message" => "nothing to update !"));
            }
        } else {
            http_response_code(404);
            echo json_encode(array("message" => "no post data"));
        }
    }
}
