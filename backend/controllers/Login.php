<?php

require "./vendor/autoload.php";
use \Firebase\JWT\JWT;

class Login
{
    public function __construct()
    {
        $this->logUser();
    }

    public function logUser()
    {
        $database = new Database();
        $db = $database->connect();

        $request = file_get_contents("php://input");
        if (isset($request) && !empty($request)) {
            $form_data = json_decode($request);
            $roll = mysqli_real_escape_string($db, trim($form_data->RollNo));
            $password = mysqli_real_escape_string($db, trim($form_data->Password));

            $encryptedPass = md5($password);
            $query = "SELECT * FROM student WHERE RollNo = '$roll' AND Password = '$encryptedPass'";
            $result = mysqli_query($db, $query);
            $count = mysqli_num_rows($result);
            if ($count == 1) {
                $row = mysqli_fetch_assoc($result);
                extract($row);
                $firstName = $row['FirstName'];
                $lastName = $row['LastName'];
                $email = $row['Email'];
                $secret_key = "YOUR_SECRET_KEY";
                $issuer_claim = "STUDENT_APP";
                $audience_claim = "THE_AUDIENCE";
                $issuedat_claim = time();
                $notbefore_claim = $issuedat_claim + 10;
                $expire_claim = $issuedat_claim + 60;
                $token = array(
                    "iss" => $issuer_claim,
                    "aud" => $audience_claim,
                    "iat" => $issuedat_claim,
                    "nbf" => $notbefore_claim,
                    "exp" => $expire_claim,
                    "data" => array(
                        "firstName" => $firstName,
                        "lastName" => $lastName,
                    ));
                http_response_code(200);
                $jwt = JWT::encode($token, $secret_key);
                echo json_encode(
                    array(
                        "message" => "Successful login",
                        "jwt" => $jwt,
                        "email" => $email,
                        "expireAt" => $expire_claim,
                    )
                );

            } else {
                http_response_code(401);
                echo json_encode(array("message" => "Login failed"));
            }

        } else {
            http_response_code(400);
            echo json_encode(array("message" => "no post data"));
        }

    }
}
