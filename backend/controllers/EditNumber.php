<?php

class EditNumber
{
    public function __construct()
    {
        $this->editNumber();
    }

    public function editNumber()
    {
        $database = new Database();
        $db = $database->connect();

        $postData = file_get_contents("php://input");
        if (isset($postData) && !empty($postData)) {
            $request = json_decode($postData);
            $RollNo = mysqli_real_escape_string($db, trim($request->RollNo));
            $NewMobileNumber = mysqli_real_escape_string($db, trim($request->NewMobileNumber));
            $query = "SELECT * FROM `student` WHERE `student`.`RollNo` = '$RollNo'";
            $result = mysqli_query($db, $query);
            $row = mysqli_fetch_array($result);
            $MobileNumber = $row["MobileNumber"];
            if ($NewMobileNumber != null && $MobileNumber != $NewMobileNumber) {
                $query1 = "UPDATE `mydb`.`student` SET `student`.`MobileNumber`='{$NewMobileNumber}' WHERE `student`.`RollNo`='{$RollNo}'";
                if (!mysqli_query($db, $query1)) {
                    http_response_code(400);
                    echo json_encode(array("message" => "unable to update"));
                } else {
                    http_response_code(201);
                    echo json_encode(array("message" => "mobile number updated"));
                }
            } else {
                http_response_code(400);
                echo json_encode(array("message" => "nothing to update !"));
            }
        } else {
            http_response_code(404);
            echo json_encode(array("message" => "no post data"));
        }
    }
}
