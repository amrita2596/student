<?php

require_once './Database.php';
require './router/route.php';

$db = new Database();
$db->connect();

$router = new Route();
