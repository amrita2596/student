<?php
//Headers
if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Access-Control-Allow-Headers: token, Content-Type');
    header('Access-Control-Max-Age: 1728000');
    header('Content-Length: 0');
    header('Content-Type: text/plain');
    die();
}

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

class Database
{
    //db params
    public $host = 'localhost';
    public $db_name = 'mydb';
    public $username = 'root';
    public $password = 'ubuntu123';
    public $conn;

    //db connect
    public function connect()
    {
        $this->conn = mysqli_connect($this->host, $this->username, $this->password, $this->db_name);
        if (mysqli_connect_errno($this->conn)) {
            die("Failed to connect:" . mysqli_connect_error());
        } else {
            mysqli_set_charset($this->conn, "utf8");
            return $this->conn;
        }
    }

    public function Close(){
        mysqli_close($conn); 
    }

}
