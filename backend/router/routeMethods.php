<?php

class RouteActions
{
    public $routes = array();
    public $methods = array();

    public function add($uri, $method = null)
    {

        $this->routes[] = '/' . trim($uri, '/');
        if ($method != null) {
            $this->methods[] = $method;
        }

    }

    public function submit()
    {
        $uriGetParam = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
        foreach ($this->routes as $key => $value) {
            if (preg_match("#^$value$#", $uriGetParam)) {
                if (is_string($this->methods[$key])) {
                    $useMethod = $this->methods[$key];
                    new $useMethod();
                } else {
                    call_user_func($this->methods[$key]);
                }
            }
        }
    }
}
