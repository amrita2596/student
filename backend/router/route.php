<?php

require 'router/routeMethods.php';
require './controllers/Register.php';
require './controllers/Login.php';
require './controllers/Dashboard.php';
require './controllers/Subjects.php';
require './controllers/EditAddress.php';
require './controllers/EditEmail.php';
require './controllers/EditNumber.php';

class Route
{
    public function route()
    {
        $router = new RouteActions();
        $router->add('/api/register', 'Register');
        $router->add('/api/login',  'Login');
        $router->add('/api/dashboard',  'Dashboard');
        $router->add('/api/subjects', 'Subjects');
        $router->add('/api/editAddress', 'EditAddress');
        $router->add('/api/editEmail', 'EditEmail');
        $router->add('/api/editNumber', 'EditNumber');
        $router->submit();
        

    }

}
