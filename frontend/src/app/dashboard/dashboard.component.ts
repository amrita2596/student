import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StudentService } from "../service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  semForm: FormGroup;
  loading = false;
  submitted = false;
  subjects: any;
  roll: any;
  Department: any = [
    "CSE",
    "ECE",
    "EE",
    "CHEM",
    "MECH",
    "CIVIL",
    "MSME",
    "ARCH",
    "MBA",
    "BCA",
    "MCA"
  ];

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.semForm = this.formBuilder.group({
      Department: ["", Validators.required],
      Semester: ["", Validators.required]
    });
  }
  get f() {
    return this.semForm.controls;
  }

  onSubmit(Status) {
    this.submitted = true;
    if (this.semForm.invalid) {
      return;
    }
    localStorage.setItem("Status", JSON.stringify(Status));
    localStorage.setItem("Department", JSON.stringify(this.f.Department.value));
    localStorage.setItem("Sem", JSON.stringify(this.f.Semester.value));
    this.roll = parseInt(localStorage.getItem("RollNo"));
    this.loading = true;
    this.studentService
      .semester(this.roll, this.f.Department.value, this.f.Semester.value)
      .subscribe(
        data => {
          this.subjects = data;
          this.router.navigate(["/api/subjects"]);
          return true;
        },
        error => {
          this.loading = false;
        }
      );
  }

  changeDepartment(e) {
    this.Department.setValue(e.target.value, {
      onlySelf: true
    });
  }

  logout() {
    this.studentService.logout();
    this.studentService
      .openNotifyDialog("You are successfully logged out !")
      .afterClosed();
    this.router.navigate(["/api/login"]);
  }

  editProfile(){
    this.router.navigate(["/api/edit"]);
  }
}
