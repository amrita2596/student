import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { User } from "./models/user";
import { MatDialog } from "@angular/material";
import { MatNotifyDialogComponent } from "./mat-notify-dialog/mat-notify-dialog.component";

@Injectable()
export class StudentService {
  constructor(
    private http: HttpClient,
    private router: Router,
    private dialog: MatDialog
  ) {}

  register(user: User) {
    return this.http.post("http://localhost:8000/api/register", user);
  }

  login(RollNo: number, Password: string) {
    return this.http.post("http://localhost:8000/api/login", {
      RollNo,
      Password
    });
  }

  semester(RollNo: number, Department: string, Semester: number) {
    return this.http.post("http://localhost:8000/api/dashboard", {
      RollNo,
      Department,
      Semester
    });
  }

  getSubjects(
    RollNo: number,
    Semester: number,
    Department: string,
    Status: string
  ) {
    return this.http.post("http://localhost:8000/api/subjects", {
      RollNo,
      Semester,
      Department,
      Status
    });
  }

  logout() {
    localStorage.clear();
  }

  loggedIn(): boolean {
    return localStorage.getItem("access-token") !== null;
  }

  openNotifyDialog(msg) {
    return this.dialog.open(MatNotifyDialogComponent, {
      width: "390px",
      panelClass: "notify-dialog-container",
      position: { top: "20px" },
      disableClose: true,
      data: {
        message: msg
      }
    });
  }

  updateAddress(RollNo: number, NewAddress: string) {
    return this.http.put("http://localhost:8000/api/editAddress", {
      RollNo,
      NewAddress
    });
  }

  updateEmail(RollNo: number, NewEmail: string) {
    return this.http.put("http://localhost:8000/api/editEmail", {
      RollNo,
      NewEmail
    });
  }

  updateNumber(RollNo: number, NewMobileNumber: string) {
    return this.http.put("http://localhost:8000/api/editNumber", {
      RollNo,
      NewMobileNumber
    });
  }
}
