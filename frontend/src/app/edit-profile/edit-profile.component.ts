import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StudentService } from "../service";

@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.component.html",
  styleUrls: ["./edit-profile.component.css"]
})
export class EditProfileComponent implements OnInit {
  editProfileForm: FormGroup;
  loading = false;
  submitted = false;
  roll: number;
  user: any;
  field: string;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.roll = parseInt(localStorage.getItem("RollNo"));
    this.editProfileForm = this.formBuilder.group({
      NewAddress: ["", Validators.required],
      NewMobileNumber: [
        "",
        [
          Validators.maxLength(10),
          Validators.required,
          Validators.minLength(10)
        ]
      ],
      NewEmail: ["", [Validators.required, Validators.email]]
    });
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.editProfileForm.controls;
  }

  UpdateAddress() {
    if (this.f.NewAddress.invalid) {
      return;
    }
    this.studentService
      .updateAddress(this.roll, this.f.NewAddress.value)
      .subscribe(
        data => {
          this.user = data;
          this.studentService
            .openNotifyDialog("Details Updated !")
            .afterClosed();
        },
        error => {
          this.studentService
            .openNotifyDialog("Cannot update the details !")
            .afterClosed();
          this.editProfileForm.reset();
        }
      );
  }

  UpdateEmail() {
    if (this.f.NewEmail.invalid) {
      return;
    }
    this.studentService.updateEmail(this.roll, this.f.NewEmail.value).subscribe(
      data => {
        this.user = data;
        this.studentService.openNotifyDialog("Details Updated !").afterClosed();
      },
      error => {
        this.studentService
          .openNotifyDialog("Cannot update the details !")
          .afterClosed();
        this.editProfileForm.reset();
      }
    );
  }

  UpdateNumber() {
    if (this.f.NewMobileNumber.invalid) {
      return;
    }
    this.studentService
      .updateNumber(this.roll, this.f.NewMobileNumber.value)
      .subscribe(
        data => {
          this.user = data;
          this.studentService
            .openNotifyDialog("Details Updated !")
            .afterClosed();
        },
        error => {
          this.studentService
            .openNotifyDialog("Cannot update the details !")
            .afterClosed();
          this.editProfileForm.reset();
        }
      );
  }
}
