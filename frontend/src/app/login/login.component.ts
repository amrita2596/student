import { Component, OnInit, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { StudentService } from "../service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  userData: any;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private studentService: StudentService
  ) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      RollNo: ["", Validators.required],
      Password: ["", Validators.required]
    });
  }
  get f() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.studentService
      .login(this.f.RollNo.value, this.f.Password.value)
      .subscribe(
        data => {
          this.userData = data;
          localStorage.setItem("RollNo", JSON.stringify(this.f.RollNo.value));
          localStorage.setItem(
            "access-token",
            JSON.stringify(this.userData.jwt)
          );
          this.router.navigate(["/api/dashboard"]);
          return true;
        },
        error => {
          this.studentService
          .openNotifyDialog("Incorrect roll no. and password !")
          .afterClosed();
          this.loading = false;
          this.loginForm.reset();
        }
      );
  }
}
