export class User {
  RollNo: number;
  FirstName: string;
  LastName: string;
  Address: string;
  MobileNumber: string;
  Email: string;
  Department: string;
  Password: string;
}
