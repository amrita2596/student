import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "./_guards/auth.guard";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import {SubjectsComponent} from "./subjects/subjects.component";
import {EditProfileComponent} from "./edit-profile/edit-profile.component";

const routes: Routes = [
  { path: "api/register", component: RegisterComponent },
  { path: "api/login", component: LoginComponent },
  {
    path: "api/dashboard",
    canActivate: [AuthGuard],
    component: DashboardComponent
  },
  {
    path: "api/subjects",
    canActivate: [AuthGuard],
    component: SubjectsComponent
  },
  {
    path: "api/edit",
    canActivate: [AuthGuard],
    component: EditProfileComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
