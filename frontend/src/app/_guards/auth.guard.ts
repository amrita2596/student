import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { StudentService } from "../service";

@Injectable({
  providedIn: "root"
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private studentService: StudentService) {}
  canActivate() {
    if (!this.studentService.loggedIn()) {
      this.router.navigate(["/api/login"]);
      return false;
    } else {
      return true;
    }
  }
}
