import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import {MatSelectModule} from '@angular/material/select';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations"
import { MatDialogModule } from "@angular/material/dialog";
import { FormsModule } from '@angular/forms';
//import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { RegisterComponent } from "./register/register.component";
import { LoginComponent } from "./login/login.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { StudentService } from "./service";
import { SubjectsComponent } from './subjects/subjects.component';
import { MatNotifyDialogComponent } from './mat-notify-dialog/mat-notify-dialog.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    SubjectsComponent,
    MatNotifyDialogComponent,
    EditProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,
     HttpClientModule,
     BrowserAnimationsModule,
     MatSelectModule
    // JwtModule.forRoot({
    //   config: {
    //     tokenGetter: function  tokenGetter() {
    //          return     localStorage.getItem('access-token');},
    //     whitelistedDomains: ['localhost:4200'],
    //     blacklistedRoutes: ['http://localhost:4200/api/login']
    //   }
    // })
  ],
  providers: [StudentService],
  bootstrap: [AppComponent],
  entryComponents: [
    RegisterComponent,
    MatNotifyDialogComponent
  ]
})
export class AppModule {}
