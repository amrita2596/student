import { Component, OnInit } from "@angular/core";
import { StudentService } from "../service";
import { Router } from "@angular/router";

@Component({
  selector: "app-subjects",
  templateUrl: "./subjects.component.html",
  styleUrls: ["./subjects.component.css"]
})
export class SubjectsComponent implements OnInit {
  Subjects: any;
  roll: number;
  Status: string;
  Department: string;
  Semester: number;

  constructor(private studentService: StudentService, private router: Router) {}

  ngOnInit() {
    this.roll = parseInt(localStorage.getItem("RollNo"));
    this.Department = JSON.parse(localStorage.getItem("Department"));
    this.Semester = JSON.parse(localStorage.getItem("Sem"));
    this.Status = JSON.parse(localStorage.getItem("Status"));
    this.studentService
      .getSubjects(this.roll, this.Semester, this.Department, this.Status)
      .subscribe(data => {
        this.Subjects = data;
      });
  }

  logout() {
    this.studentService.logout();
    this.studentService
      .openNotifyDialog("You are successfully logged out !")
      .afterClosed();
    this.router.navigate(["/api/login"]);
  }

  back(){
    this.router.navigate(["/api/dashboard"]);
  }
}
